# Práctica NLP

El objetivo principal de la práctica es realizar tres ejercicios relacionados con NLP:

- Modelado de topics
- Análisis de sentimiento
- NLG

El enunciado original de la práctica se encuentra en _Práctica Final NLP.ipynb_.

# Modelado de topics

Para el modelado de topics se han analizado cinco archivos de reviews de Amazon, que se encuentran bajo la carpeta _data_. Estos archivos se bajaron de http://jmcauley.ucsd.edu/data/amazon/, de los correspondientes enlaces 5-core.

El ejercicio está desarrollado en el notebook _Práctica-NLP-JoséÁngelArrechea-Modelado-de-Topics.ipynb_

En este notebook, además de los resultados obtenidos, se generaron dos html que muestran los topics encontrados entre los reviews de Amazon, que se encuentran bajo la carpeta _results/modeling-topics_.

- topics_vis_5.html: Gráfico generado con pyLDAvis con información sobre 5 topics encontrados en la reviews de Amazon.
  ![topics_vis_5](results/modeling-topics/topics_vis_5.png)
- topics_vis_7.html: Gráfico generado con pyLDAvis con información sobre 7 topics encontrados en la reviews de Amazon.
  ![topics_vis_7](results/modeling-topics/topics_vis_7.png)

# Análisis de sentimiento

Se realiza un análisis de sentimientos sobre los mismos datos que se han procesado en el ejercicio de modelado de topics.

El ejercicio está desarrollado en el notebook _Práctica-NLP-JoséÁngelArrechea-Análisis-de-Sentimiento.ipynb_.

# NLG. Generación de tweets.

En este ejercicio de se realizó a partir de los tweets de Barack Obama, que se obtuvieron de https://www.kaggle.com/speckledpingu/RawTwitterFeeds?select=BarackObama.csv. El archivo descargado se encuentra en la carpeta _data_.

El ejercicio está desarrollado en el notebook _Práctica-NLP-JoséÁngelArrechea-Generación-Tweets.ipynb_. También se encuentra en Google Colab en https://colab.research.google.com/drive/1MxdGQl6wC6lEIlH0OjZZSmj6OHZShKzb?usp=sharing	 

En el ejercicio se crearon nueve modelos con sus correspondiente pesos. Los archivos con los pesos se encuentra bajo la carpeta _results/nlg_.

Los cuatro primeros modelos se generaron a partir del texto en crudo de los tweets, sin ninguna transformación previa. Los archivos son:

- model_1_sin_filtro.h5
- model_2_sin_filtro.h5
- model_3_sin_filtro.h5
- model_4_sin_filtro.h5

Se crearon otros cuatro a partir del texto transformado de los tweets, en el que eliminaron menciones a usuarios, hashtag, enlaces, etc. Los archivos son:

- model_5_con_filtro.h5
- model_6_con_filtro.h5
- model_7_con_filtro.h5
- model_8_con_filtro.h5

Los archivos hasta ahora mencionados se obtuvieron ejecutando el notebook en Google Colab.

Debido a la lentitud de procesado y los malos resultados, se decidió ejecutar uno de los modelos propuestos, pero con más neuronas y épocas, en un ordenador físico. Los pesos del modelo obtenido se encuentran en:

- model_9_con_filtro_20.h5

